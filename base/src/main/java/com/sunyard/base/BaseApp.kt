package com.sunyard.base

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import androidx.multidex.MultiDex
import com.blankj.utilcode.util.LogUtils
import com.sunyard.base.ktExt.*

import com.sunyard.base.util.Utils

open class BaseApp:Application() {

    override fun onCreate() {
        super.onCreate()
        Utils.init(this)
        initLanguage()
        LogUtils.i("初始化 App")
    }

    override fun onTerminate() {
        super.onTerminate()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        onLanguageConfigurationChanged()
    }
}