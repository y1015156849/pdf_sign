package com.sunyard.base.api



import com.sunyard.base.entity.Test
import com.sunyard.base.ktExt.BaseResp
import retrofit2.http.GET

interface DemoApiService {
    @GET("https://www.apiopen.top/novelApi")
    suspend fun getTestApi(): BaseResp<List<Test>>
}