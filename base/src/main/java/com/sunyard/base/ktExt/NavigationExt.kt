package com.sunyard.base.ktExt

import android.app.Activity
import android.view.View
import androidx.navigation.Navigation

fun View.go(navId: Int){
    Navigation.findNavController(this).navigate(navId)
}
fun Activity.go(navViewId:Int,navId: Int){
    Navigation.findNavController(this,navViewId).navigate(navId)
}





