package com.sunyard.base.ktExt

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log


import java.util.*

/**
 * package: com.test.util
 * Author: 杨振宇 1015156849@qq.com
 * Date: 2019/5/20 10:00
 * Description: 语言扩展
 */

open class LanguageExt {
    var currentLocale: Locale? = null
     val activityStack:Stack<Activity> = Stack()

    companion object {
        /**双重校验锁式*/
        val instance: LanguageExt by lazy(mode = LazyThreadSafetyMode.SYNCHRONIZED) {
            LanguageExt()
        }
    }
}

fun Application.initLanguage() {
    Log.d("TEST", "初始化多国语言")
    Log.d("TEST", getCurrentLocale().language)
    registerActivityLifecycleCallbacks(LanguageActivityLifecycleCallbacks())

    getCurrentLocale()
}

fun Context.getCurrentLocale(): Locale {

    if (LanguageExt.instance.currentLocale == null) {
        LanguageExt.instance.currentLocale =
            Locale(getSP("LOCALE", Locale.getDefault().language))
    }
    return LanguageExt.instance.currentLocale!!
}

fun Context.changeCurrentLocale(locale: Locale, save: Boolean) {

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
        resources.configuration.setLocale(locale)
    } else {
        resources.configuration.locale = locale
    }
    resources.updateConfiguration(resources.configuration, resources.displayMetrics)
    LanguageExt.instance.currentLocale = locale

    if (save) {
        setSP("LOCALE", locale.language)
        LanguageExt.instance.activityStack.forEach {
            it.recreate()
        }
    }

}

fun Application.onLanguageConfigurationChanged() {
    changeCurrentLocale(
        getCurrentLocale(),
        false
    )
}

class LanguageActivityLifecycleCallbacks : Application.ActivityLifecycleCallbacks {
    override fun onActivityPaused(p0: Activity?) {}

    override fun onActivityResumed(p0: Activity?) {}

    override fun onActivityStarted(p0: Activity?) {}

    override fun onActivityDestroyed(p0: Activity?) {
        LanguageExt.instance.activityStack.remove(p0)
    }

    override fun onActivitySaveInstanceState(p0: Activity?, p1: Bundle?) {}

    override fun onActivityStopped(p0: Activity?) {}

    override fun onActivityCreated(p0: Activity?, p1: Bundle?) {
        Log.d("TEST", "onActivityCreate => currentLocale = ${p0?.getCurrentLocale()?.language}")
        p0?.changeCurrentLocale(
            p0.getCurrentLocale(),
            false
        )
        LanguageExt.instance.activityStack.add(p0)
    }


}


