package com.sunyard.base.ktExt

import android.app.Activity
import com.yanzhenjie.permission.AndPermission
import java.io.File
import android.content.DialogInterface
import android.app.AlertDialog



fun Activity.getPermission(permissionsArray: Array<String>, success:()->Unit, fail:()->Unit){
    AndPermission.with(this)
        .runtime()
        .permission(permissionsArray)
        .onGranted {
            //获取成功
            success()
        }
        .onDenied {
            //获取失败
            fail()
        }

        .start()

}

fun Activity.getPermissionInstall(file: File, success:()->Unit, fail:()->Unit){
    AndPermission.with(this)
        .install()
        .file(file)
        .onGranted {
            //获取成功
            success()
        }
        .onDenied {
            //获取失败
            fail()
        }

        .start()

}

fun Activity.getPermissionNotification(success:()->Unit, fail:()->Unit){
    AndPermission.with(this)
        .notification()
        .listener()
        .rationale { context, data, executor ->
            // Startup settings: e.execute();
            // Cancel: e.cancel();

            AlertDialog.Builder(context).setCancelable(false)
                .setTitle("请求权限")
                .setMessage("请允许显示通知")
                .setPositiveButton("设置",
                    DialogInterface.OnClickListener { dialog, which ->
                        AndPermission.with(this).runtime().setting().start(1)
//                        executor.execute()
                        })
                .setNegativeButton("取消",
                    DialogInterface.OnClickListener { dialog, which -> executor.cancel() })
                .show()

        }
        .onGranted {

            //获取成功
            success()
        }
        .onDenied {
            //获取失败
            fail()
        }

        .start()
}