package com.sunyard.base.ktExt

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProviders
import com.sunyard.base.base.BaseActivity
import com.sunyard.base.base.BaseFragment
import com.sunyard.base.viewModel.BaseViewModel


fun  <T : BaseViewModel> BaseActivity<T>.getViewModel(cls:Class<T>):T{
    return ViewModelProviders.of(this).get(cls)
}

fun  <T : BaseViewModel> BaseFragment<T>.getViewModel(cls:Class<T>):T{
    return ViewModelProviders.of(this).get(cls)
}
//绑定activity
fun  <T : BaseViewModel> BaseFragment<T>.getViewModelBindActivity(cls:Class<T>):T{
    return ViewModelProviders.of(activity!!).get(cls)
}

fun <T> MutableLiveData<T>.setNewParams(data:(it:T?)->Unit){
    val newData=this.value


    data(newData)
    this.value=newData
}

fun <T> MutableLiveData<T>.setNewData(data:()->T?){
    this.value=data()
}