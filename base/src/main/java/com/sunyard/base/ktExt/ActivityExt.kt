package com.sunyard.base.ktExt

import android.app.Activity
import android.content.Intent
import androidx.fragment.app.Fragment

fun<K> Activity.startActivity(cls : Class<K>){
    val intent= Intent()
    intent.setClass(this,cls)
    startActivity(intent)
}

fun<K> Activity.startActivity(cls : Class<K>,data:String){
    val intent= Intent()
    intent.setClass(this,cls)
    intent.putExtra("data",data)
    startActivity(intent)
}

fun<K> Fragment.startActivity(cls : Class<K>){
   activity?.startActivity(cls)
}

fun<K> Fragment.startActivity(cls : Class<K>,data:String){
    activity?.startActivity(cls,data)
}