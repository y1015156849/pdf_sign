package com.sunyard.base.ktExt

open class DataExt {
    companion object {

        /**双重校验锁式*/
        val instance: DataExt by lazy(mode = LazyThreadSafetyMode.SYNCHRONIZED) {
            DataExt()
        }
    }
}

//外层实体
data class BaseResp<T>(
    var code: Int = 0,
    var msg: String = "",
    var `data`: T?
)

/*数据解析扩展函数*/
fun <T> BaseResp<T>.dataConvert(): T? {

    if (code == 200) {
        return data
    } else {
        throw Exception(msg)
    }

}

/*数据解析扩展函数*/
/**
 * @param success code=0 的成功请求，直接拿数据处理
 * @param exception code在exceptionCode 数组的内，不为0，且需要特殊处理的情况下使用
 * @param fail code 既不是0也不需要特殊处理，只管显示toast
 * */
fun <T> BaseResp<T>.resultConvert(
    finally: (msg: String) -> Unit,
    success: (data: T?) -> Unit,
    exception: (code: Int, msg: String) -> Unit,
    fail: (msg: String) -> Unit
) {
    finally(msg)
    when (code) {
        0 -> success(data)
        else -> fail(msg)
    }

}


fun String?.toNull2String(): String {
    return if (this == null || this == "null" || this == "Null" || this == "NULL") {
        ""
    } else {
        this
    }

}


