package com.sunyard.base.ktExt

import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyPermanentlyInvalidatedException
import android.security.keystore.KeyProperties
import android.widget.Toast
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.blankj.utilcode.util.LogUtils
import com.sunyard.base.util.CipherHelper
import java.io.IOException
import java.security.*
import java.security.cert.CertificateException
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.NoSuchPaddingException
import javax.crypto.SecretKey


/**
 * 生物识别扩展类
 * */
interface BiometricVerifyCallBack {
    fun onSuccess()
}


fun FragmentActivity.showBiometricPrompt(callback: BiometricVerifyCallBack) {

    try {
        val promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("生物认证")
            .setConfirmationRequired(true)
            .setSubtitle("请验证您的生物信息")
            .setNegativeButtonText("取消")
            .build()

        val biometricPrompt = BiometricPrompt(this, ContextCompat.getMainExecutor(this),
            object : BiometricPrompt.AuthenticationCallback() {
                /**在遇到不可恢复的错误且操作完成时调用。
                 *不会对此对象执行进一步操作。*/
                override fun onAuthenticationError(
                    errorCode: Int,
                    errString: CharSequence
                ) {
                    super.onAuthenticationError(errorCode, errString)
                    LogUtils.e("onAuthenticationError")
                    /**没录入指纹的时候*/
//                    Toast.makeText(
//                        applicationContext,
//                        "验证失败 : $errString", Toast.LENGTH_SHORT
//                    )
//                        .show()
                }

                override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult
                ) {
                    super.onAuthenticationSucceeded(result)
                    callback.onSuccess()
                }

                /**
                 * 当生物特征有效但无法识别时调用。
                 * */
                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    LogUtils.e("onAuthenticationFailed")
                    Toast.makeText(
                        applicationContext, "验证失败",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            })

        biometricPrompt.authenticate(promptInfo)
    } catch (e: Exception) {
        e.printStackTrace()

    }

}

fun FragmentActivity.isSupportFaceBio(): Boolean {
//    return when (BiometricManager.from(this).canAuthenticate()) {
//        /**未检测到错误*/
//        BiometricManager.BIOMETRIC_SUCCESS -> {
//            true
//        }
//        /**没有生物识别硬件*/
//        BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
//            false
//        }
//        /**硬件不可用。请稍后再试。*/
//        BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
//            false
//        }
//        /**用户没有注册任何生物特征。*/
//        BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
//            false
//        }
//        else -> {
//            false
//        }
//    }
    return false
//    return Build.VERSION.SDK_INT > Build.VERSION_CODES.Q;
}

fun FragmentActivity.isSupportFingerPrintBio(): Boolean {
    return when (BiometricManager.from(this).canAuthenticate()) {
        /**未检测到错误*/
        BiometricManager.BIOMETRIC_SUCCESS -> {
            true
        }
        /**没有生物识别硬件*/
        BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
            false
        }
        /**硬件不可用。请稍后再试。*/
        BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
            false
        }
        /**用户没有注册任何生物特征。*/
        BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
            false
        }
        else -> {
            false
        }
    }


}

fun FragmentActivity.isSupportGestureBio(): Boolean {
    return true
}


