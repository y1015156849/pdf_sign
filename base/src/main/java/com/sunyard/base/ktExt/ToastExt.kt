package com.sunyard.base.ktExt

import android.app.Activity
import android.widget.Toast
import androidx.fragment.app.Fragment

fun Activity.toast(s:String){
    Toast.makeText(this,s,Toast.LENGTH_SHORT).show()
}

fun Activity.toastLong(s:String){
    Toast.makeText(this,s,Toast.LENGTH_LONG).show()
}

fun Fragment.toast(s:String){
    Toast.makeText(activity,s,Toast.LENGTH_SHORT).show()
}

fun Fragment.toastLong(s:String){
    Toast.makeText(activity,s,Toast.LENGTH_LONG).show()
}