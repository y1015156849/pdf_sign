package com.sunyard.base.widget;

/**
 * package: com.yzy.weight
 * Author: 杨振宇 1015156849@qq.com
 * Date: 2019/3/21 13:12
 * Description:
 */

import java.text.SimpleDateFormat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class CountdownView extends AppCompatTextView {

    private String countdownTime;
    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat dateFormat = new SimpleDateFormat();
    private final long MINUTES = 1000 * 60;
    private final long HOURS = MINUTES * 60;
    private final long DAY = HOURS * 24;
    private String match = "";
    private int ruler = -1000;//规则 增减秒数
    private String before = "";
    private String after = "";
    private boolean openDownToUp = false;//默认不从倒计时转成正计时
    private int color=0;

    public String getBefore() {
        return before;
    }

    public void setBefore(String before) {
        this.before = before;
    }

    public String getAfter() {
        return after;
    }

    public void setAfter(String after) {
        this.after = after;
    }

    private TimeFinishListener timeFinishListener;

    public CountdownView(Context context) {
        this(context, null);
    }

    public CountdownView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CountdownView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            long time = Long.valueOf(countdownTime);
            if (ruler < 0) {//倒计时就算到0
                if (time > 0) {
                    setText(match(time) + match);
                    handler.sendEmptyMessageDelayed(0, 1000);
                } else {
                    setText(match(0) + match);
                    if (!openDownToUp) {
                        if (timeFinishListener != null) {
                            timeFinishListener.timeFinish(time);
                        }
                        reset();
                        return;
                    }else{
                        ruler=-ruler;
                        setTextColor(color);
                    }

                }
            } else {//正计时 不会到0 一直计时 需要手动reset停止
                setText(before + match(Math.abs(time)) + after);
                handler.sendEmptyMessageDelayed(0, 1000);
            }
            countdownTime = String.valueOf(Math.abs(time) + ruler);
        }

    };

    public void addTimeFinishListener(TimeFinishListener timeFinishListener) {
        this.timeFinishListener = timeFinishListener;
    }

    public void openDownToUp(boolean openDownToUp,int Color) {
        this.openDownToUp = openDownToUp;
        this.color=color;
    }

    public void setMatch(String match) {
        this.match = match;
    }

    public String getCountdownStr() {
        return countdownTime;
    }

    //计算剩余时间
    public String getRestTime(Long time) {
        return time - System.currentTimeMillis() + "";
    }

    public void setTime(String countdownStr) {
        reset();

        this.countdownTime = countdownStr;
        handler.sendEmptyMessage(0);
    }

    public void setRuler(int ruler) {
        this.ruler = ruler;
    }

    public void reset() {
        handler.removeMessages(0);
        countdownTime = null;
    }


    public static String match(long time) {

//        time = time/1000;
        int day = (int) (time / (3600000 * 24));
        int hour = (int) ((time % (3600000 * 24)) / 3600000);
        int min = (int) (time % 3600000 / 60000);
        int s = (int) (time % 3600000 % 60000 / 1000);

//        String dayStr = day + "天";
        String hourStr = String.format("%02d:", (day * 24) + hour);
        String minStr = String.format("%02d:", min);
        String sStr = String.format("%02d", s);

        String str = "";

//        if (day > 0) {
//            str = str + dayStr;
//        }
        if (hour >= 0) {
            str = str + hourStr;
        }
        if (min >= 0) {
            str = str + minStr;
        }
        if (s >= 0) {
            str = str + sStr;
        }

        return str;
    }
//    private String match(long num){
//        int hour = (int)(num % (3600 * 24) / 3600);
//        int min = (int)(num % 3600 / 60);
//        int s = (int)(num % 3600 % 60);
//
//        String hourStr = String.format("%02d:", hour);
//        String minStr = String.format("%02d:", min);
//        String sStr = String.format("%02d", s);
//
//        String str = "";
//
//        if (hour >= 0) {
//            str += hourStr;
//        }
//        if (min >= 0) {
//            str += minStr;
//        }
//        if (s >= 0) {
//            str += sStr;
//        }
//
//        return str;
//    }


    public interface TimeFinishListener {
        void timeFinish(long time);
    }

}

