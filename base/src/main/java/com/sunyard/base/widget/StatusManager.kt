package com.sunyard.base.widget

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager

import android.view.View
import android.view.ViewGroup


import androidx.annotation.DrawableRes
import androidx.annotation.RequiresPermission

import androidx.fragment.app.FragmentActivity


import android.Manifest.permission.ACCESS_NETWORK_STATE
import com.sunyard.base.base.hjq.BaseDialog
import com.sunyard.base.widget.dialog.WaitDialog
import com.sunyard.base.R

/**
 * desc   : 界面状态管理类
 */
class StatusManager {



    // 提示布局
    private var mHintLayout: HintLayout? = null
    // 加载对话框
    private var mDialog: BaseDialog? = null

    /**
     * 显示加载中
     */
    fun showLoading(activity: FragmentActivity) {
        if (mDialog == null) {
            mDialog = WaitDialog.Builder(activity)
                .setMessage("加载中...") // 消息文本可以不用填写
                .create()
        }

        if (!mDialog!!.isShowing) {
            mDialog!!.show()
        }
    }

    /**
     * 显示加载完成
     */
    fun showComplete() {

        if (mDialog != null && mDialog!!.isShowing) {
            mDialog!!.dismiss()
        }

        if (mHintLayout != null && mHintLayout!!.isShow) {
            mHintLayout!!.hide()
        }
    }

    /**
     * 显示空提示
     */
    fun showEmpty(view: View) {
        showLayout(view, R.drawable.icon_no_data, "暂无数据");
    }

    /**
     * 显示错误提示
     */
    fun showError(view: View) {
        // 判断当前网络是否可用
        if (isNetworkAvailable(view.context)) {
            showLayout(view, R.drawable.icon_no_data, "请求出错了");
        } else {
            showLayout(view, R.drawable.icon_no_data, "没有网络了");
        }
    }

    fun hasNetError(){

    }

    /**
     * 显示自定义提示
     */
    fun showLayout(view: View, @DrawableRes iconId: Int,  textStr: String) {
        showLayout(view, view.resources.getDrawable(iconId), textStr)
    }

    fun showLayout(view: View, drawable: Drawable, hint: CharSequence) {
        if (mDialog != null && mDialog!!.isShowing) {
            mDialog!!.dismiss()
        }
        if (mHintLayout == null) {

            if (view is HintLayout) {
                mHintLayout = view
            } else if (view is ViewGroup) {
                mHintLayout = findHintLayout(view)
            }
//              LogUtils.e("You didn't add this HintLayout to your Activity layout")
//            checkNotNull(mHintLayout) { "You didn't add this HintLayout to your Activity layout" }
        }
        mHintLayout!!.show()
        mHintLayout!!.setIcon(drawable)
        mHintLayout!!.setHint(hint)
    }

    /**
     * 判断网络功能是否可用
     */
    @RequiresPermission(ACCESS_NETWORK_STATE)
    private fun isNetworkAvailable(context: Context): Boolean {
        val info =
            (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo
        return info != null && info.isConnected
    }

    /**
     * 智能获取布局中的 HintLayout 对象
     */
    private fun findHintLayout(group: ViewGroup): HintLayout? {
        for (i in 0 until group.childCount) {
            val view = group.getChildAt(i)
            if (view is HintLayout) {
                return view
            } else if (view is ViewGroup) {
                val layout = findHintLayout(view)
                if (layout != null) return layout
            }
        }
        return null
    }
}