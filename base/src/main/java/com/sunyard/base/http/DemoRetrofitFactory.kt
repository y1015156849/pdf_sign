package com.sunyard.base.http

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DemoRetrofitFactory :BaseRetrofitFactory(){
     override fun addConverterFactory(retrofitBuilder: Retrofit.Builder) {
         retrofitBuilder.addConverterFactory(GsonConverterFactory.create())
     }

     companion object {

         val instance: DemoRetrofitFactory by lazy(mode = LazyThreadSafetyMode.SYNCHRONIZED) {
             DemoRetrofitFactory()
         }

     }
     override fun addInterceptor(okHttpClientBuilder: OkHttpClient.Builder) {

     }
 }





