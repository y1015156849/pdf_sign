package com.sunyard.base.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {
    val msg = MutableLiveData<String>()
    val showMsgOK = MutableLiveData<Boolean>(false)

    val code = MutableLiveData<Int>()
    val showCodeOK = MutableLiveData<Boolean>(false)

    fun showMsg(msg: String?) {
        showMsgOK.value = false
        if (msg != null) {
            this.msg.value = msg
        } else {
            this.msg.value = "请求异常"
        }

    }

    fun showErrorView(code: Int, errMsg: String?) {
        showCodeOK.value = false
        this.code.value = code
        showMsg(errMsg)
    }
}