package com.sunyard.base.util.progress;

import android.app.Dialog;
import android.content.Context;
import android.widget.TextView;

import com.sunyard.base.R;
import com.sunyard.base.widget.dialog.MessageDialog;


/**
 * Created by wang-pc on 2018/8/16.
 */
public class Utils {
    private Dialog progressDialog;
    private MessageDialog myDialog;
    private static Utils utils;

    public static Utils getInstance() {
        if (utils == null) {
            utils = new Utils();
        }
        return utils;
    }

    public void showLoadingDialog(Context context) {
        if (progressDialog != null && progressDialog.isShowing()) {
            return;
        }
        progressDialog = new Dialog(context, R.style.progress_dialog);
        progressDialog.setContentView(R.layout.dialog_h);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView msg = (TextView) progressDialog.findViewById(R.id.id_tv_loadingmsg);
        msg.setText("卖力加载中");
        progressDialog.show();
    }

    public void dismissLoadingDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
