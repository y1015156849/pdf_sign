package com.sunyard.base.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * SharedPreferences工具类
 * 用于储存
 * 登陆token，用户名，
 */

public class PreferencesUtils {

    //获得SharedPreferences实例
    private static final SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    //保存字符串
    public static void putString(Context context, String key, String value) {
        //获得SharedPreferences实例
        SharedPreferences preferences = getPreferences(context);
        //获得SharedPreferences.Editor实例
        SharedPreferences.Editor editor = preferences.edit();
        //存储数据
        editor.putString(key, value);
        //提交
        editor.commit();
    }

    //读取字符串
    public static String getString(Context context, String key) {
        //获得SharedPreferences实例
        SharedPreferences preferences = getPreferences(context);
        return preferences.getString(key, "");
    }

    //保存数字
    public static void putInteger(Context context, String key, int value) {
        //获得SharedPreferences实例
        SharedPreferences preferences = getPreferences(context);
        //获得SharedPreferences.Editor实例
        SharedPreferences.Editor editor = preferences.edit();
        //存储数据
        editor.putInt(key, value);
        //提交
        editor.commit();
    }

    //读取数字
    public static int getInteger(Context context, String key) {
        //获得SharedPreferences实例
        SharedPreferences preferences = getPreferences(context);
        return preferences.getInt(key, 0);
    }

    //保存字符串
    public static void putBoolean(Context context, String key, boolean value) {
        //获得SharedPreferences实例
        SharedPreferences preferences = getPreferences(context);
        //获得SharedPreferences.Editor实例
        SharedPreferences.Editor editor = preferences.edit();
        //存储数据
        editor.putBoolean(key, value);
        //提交
        editor.commit();
    }

    //读取字符串
    public static boolean getBoolean(Context context, String key) {
        //获得SharedPreferences实例
        SharedPreferences preferences = getPreferences(context);
        return preferences.getBoolean(key, false);
    }

    public static void clear(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("name", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();

        editor.commit();
    }

}
